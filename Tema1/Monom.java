public class Monom implements Comparable<Monom> 
{
	private Integer coef;
	private Integer grad;
	
	public Monom() 
	{
		this.coef = 0;
		this.grad = 0;
	}
	
	public Monom(int termenLiber) 
	{
		this.coef = termenLiber;
		this.grad = 0;
	}
	
	public Monom(int coef, int grad) 
	{
		this.grad = grad;
		this.coef = coef;
	}
	
	public Integer getCoef() 
	{
		return this.coef;
	}
	
	public Integer getGrad() 
	{
		return this.grad;
	}
	
	public void setCoef(int coef) 
	{
		this.coef = coef;
	}
	
	public void setGrad(int grad) 
	{
		this.grad = grad;
	}
	
	public void set(int coef, int grad) 
	{
		this.grad = grad;
		this.coef = coef;
	}
	
	public String toString() 
	{
		String aux = new String();
		if(!(this.getCoef().equals(1)) && !(this.getCoef().equals(-1)) && !(this.getCoef().equals(0))) 
		{
			if(!(this.getGrad().equals(0)) && !(this.getGrad().equals(1))) 
			{
				aux = this.getCoef() + "x^" + this.getGrad();
			}
			if(this.getGrad().equals(0)) 
			{
				aux = this.getCoef().toString();
			}
			if(this.getGrad().equals(1)) 
			{
				aux = this.getCoef() + "x";
			}
		}
		if(this.getCoef().equals(1))
		{
			if(!(this.getGrad().equals(0)) && !(this.getGrad().equals(1))) 
			{
				aux ="x^" + this.getGrad();
			}
			if(this.getGrad().equals(0)) 
			{
				aux = "1";
			}
			if(this.getGrad().equals(1)) 
			{
				aux ="x";
			}
		}
		if(this.getCoef().equals(-1))
		{
			if(!(this.getGrad().equals(0)) && !(this.getGrad().equals(1))) 
			{
				aux ="-x^" + this.getGrad();
			}
			if(this.getGrad().equals(0)) 
			{
				aux = "-1";
			}
			if(this.getGrad().equals(1)) 
			{
				aux ="-x";
			}
		}
		if(this.getCoef().equals(0))
		{
			aux = " ";
		}
		return aux;
	}
	
	
	@Override
	public int compareTo(Monom mon) 
	{
		return mon.getGrad() - this.getGrad();
	}
}
