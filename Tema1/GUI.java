import java.awt.event.*;
import java.awt.GridLayout;

import javax.swing.*;

public class GUI {
	public static void main(String args[])
	{
		JFrame frame = new JFrame("Polynomials");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(700, 300);  
		
		JPanel panel = new JPanel();
		GridLayout MyLayout = new GridLayout(5, 2);
		panel.setLayout(MyLayout);
		
		JTextField inputPolinom1 = new JTextField();
		panel.add(inputPolinom1);
		
		JTextField inputPolinom2 = new JTextField();
		panel.add(inputPolinom2);
		
		JButton adunare = new JButton("+");
		JButton scadere = new JButton("-");
		JButton inmultire = new JButton("*");
		JButton impartire = new JButton("/");
		JButton derivare = new JButton("derivare");
		JButton integrare = new JButton("integrare");
		
		
		panel.add(adunare);
		panel.add(scadere);
		panel.add(inmultire);
		panel.add(impartire);
		panel.add(derivare);
		panel.add(integrare);
		
		JTextField rezultat = new JTextField();
		panel.add(rezultat);
		
		JTextField rest = new JTextField();
		panel.add(rest);
		
		adunare.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String sPol1 = inputPolinom1.getText();
				String sPol2 = inputPolinom2.getText();
				Polinom pol1 = new Polinom();
				pol1.toPolinom(sPol1);
				Polinom pol2 = new Polinom();
				pol2.toPolinom(sPol2);
				
				rezultat.setText(pol1.adunare(pol2).toString());
			}
		});
		
		scadere.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String sPol1 = inputPolinom1.getText();
				String sPol2 = inputPolinom2.getText();
				Polinom pol1 = new Polinom();
				pol1.toPolinom(sPol1);
				Polinom pol2 = new Polinom();
				pol2.toPolinom(sPol2);
				
				rezultat.setText(pol1.scadere(pol2).toString());
			}
		});
		
		inmultire.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String sPol1 = inputPolinom1.getText();
				String sPol2 = inputPolinom2.getText();
				Polinom pol1 = new Polinom();
				pol1.toPolinom(sPol1);
				Polinom pol2 = new Polinom();
				pol2.toPolinom(sPol2);
				
				rezultat.setText(pol1.inmultire(pol2).toString());
			}
		});
		
		impartire.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String sPol1 = inputPolinom1.getText();
				String sPol2 = inputPolinom2.getText();
				Polinom pol1 = new Polinom();
				pol1.toPolinom(sPol1);
				Polinom pol2 = new Polinom();
				pol2.toPolinom(sPol2);
				
				rezultat.setText(pol1.impartire(pol2).getCat().toString());
				rest.setText(pol1.impartire(pol2).getRest().toString());
			}
		});
		
		derivare.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String sPol1 = inputPolinom1.getText();
				Polinom pol1 = new Polinom();
				pol1.toPolinom(sPol1);
				
				rezultat.setText(pol1.derivare().toString());
			}
		});
		
		integrare.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String sPol1 = inputPolinom1.getText();
				Polinom pol1 = new Polinom();
				pol1.toPolinom(sPol1);
				
				rezultat.setText(pol1.integrare().toString());
			}
		});
		
		frame.setContentPane(panel);
		frame.setVisible(true);
	}
}
