import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom 
{
    private ArrayList<Monom> monoame = new ArrayList<Monom>();
    
    public Polinom() 
    {
    	Monom mon = new Monom();
    	monoame.add(mon);
    }
    
    public void add(int coef, int grad) 
    {
    	Monom aux = new Monom(coef, grad);
    	this.monoame.add(aux);
    	this.rearanjare();
    }
    
    public void add(Monom mon) 
    {
    	this.monoame.add(mon);
    	this.rearanjare();
    }
    
    public Polinom adunare(Polinom pol) 
    {
    	Polinom rezultat = new Polinom();
    	
    	for(int i = 0; i < this.monoame.size(); i++) 
    	{
    		Monom aux = new Monom(this.monoame.get(i).getCoef(), this.monoame.get(i).getGrad());
    		rezultat.monoame.add(aux);
    	}
    	for(int i = 0; i < pol.monoame.size(); i++) 
    	{
    		Monom aux = new Monom(pol.monoame.get(i).getCoef(), pol.monoame.get(i).getGrad());
    		rezultat.monoame.add(aux);
    	}
    	
    	
    	
    	rezultat.rearanjare();
    	return rezultat;
    }
    
    public Polinom scadere(Polinom pol) 
    {
    	Polinom rezultat = new Polinom();
    	
    	for(int i = 0; i < this.monoame.size(); i++) {
    		Monom aux = new Monom(this.monoame.get(i).getCoef(), this.monoame.get(i).getGrad());
    		rezultat.monoame.add(aux);
    	}
    	for(int i = 0; i < pol.monoame.size(); i++) {
    		Monom aux = new Monom(pol.monoame.get(i).getCoef() * (-1), pol.monoame.get(i).getGrad());
    		rezultat.monoame.add(aux);
    	}
    	
    	
    	rezultat.rearanjare();
    	return rezultat;
    }
    
    public void rearanjare() 
    {
    	Collections.sort(this.monoame);
    	
    	int i = 0;
    	while(i < this.monoame.size())
    	{
    		if(this.monoame.get(i).getCoef().equals(0))
    		{
    			if(this.monoame.size() > 1)
    			{
    				this.monoame.remove(i);
    			}
    			else
    			{
    				i++;
    			}
    	 	}
    		else
    		{
    			i++;
    		}
    	}
    	
    	i = 0;
    	while(i < this.monoame.size() - 1)
    	{
    		if(this.monoame.get(i).getGrad().equals(this.monoame.get(i + 1).getGrad()))
    		{
    			this.monoame.get(i).setCoef(this.monoame.get(i).getCoef() + this.monoame.get(i + 1).getCoef());
    			this.monoame.remove(i+1);
    		}
    		else
    		{
    			i++;
    		}
    	}
    }
    
    public Polinom inmultire(Polinom pol) 
    {
    	Polinom rezultat = new Polinom();
    	
    	int i = 0;
    	
    	while(i < this.monoame.size()) 
    	{
    		int j = 0;
    		while(j < pol.monoame.size()) 
    		{
        		Monom aux = new Monom(this.monoame.get(i).getCoef() * pol.monoame.get(j).getCoef(),
        				this.monoame.get(i).getGrad() + pol.monoame.get(j).getGrad());
        		rezultat.monoame.add(aux);
        		j++;
    		}
    		i++;
    	}
    	
    	rezultat.rearanjare();
    	
    	return rezultat;
    }
    
    public RezultatImpartire impartire(Polinom pol) 
    {
    	this.rearanjare();
    	pol.rearanjare();
    	
    	Polinom cat = new Polinom();
    	Polinom rest = new Polinom();
    	
    	Monom aux = new Monom(this.monoame.get(0).getCoef() / pol.monoame.get(0).getCoef(),
				this.monoame.get(0).getGrad() - pol.monoame.get(0).getGrad());
    	cat.monoame.add(aux);
    	rest = this.scadere(cat.inmultire(pol));
    	
	   	while(rest.monoame.size()!= 0 && rest.monoame.get(0).getGrad().compareTo(pol.monoame.get(0).getGrad()) >= 0)
	   	{
	   		aux = new Monom(rest.monoame.get(0).getCoef() / pol.monoame.get(0).getCoef(),
	   				rest.monoame.get(0).getGrad() - pol.monoame.get(0).getGrad());
    		cat.monoame.add(aux);
    		Polinom polAux = new Polinom();
    		polAux.monoame.add(aux);
    		rest = rest.scadere(pol.inmultire(polAux));
	    }
    	
    	RezultatImpartire rezultat = new RezultatImpartire(cat, rest);
    	
    	if(rest.monoame.size() == 0)
    	{
    		rest.add(0, 0);
    	}
    	
    	cat.rearanjare();
    	rest.rearanjare();
    	return rezultat;
    }
    
    public String toString() 
    {
    	this.rearanjare();
    	String aux = new String();
    	
    	aux = this.monoame.get(0).toString();
    	
    	for(int i = 1; i < this.monoame.size(); i++)
    	{
    		if(this.monoame.get(i).getCoef() > 0)
    		{
    		aux = aux + "+" + this.monoame.get(i).toString();
    		}
    		else
    		{
    			aux = aux + this.monoame.get(i).toString();
    		}
    	}
    	return aux;
    }
    
    public Polinom toPolinom(String pPol)
    {
    	Pattern pattern = Pattern.compile("([+-]?(?:(?:\\d)|(?:\\d+x)|(?:\\d+)|(?:x)))");
    	Pattern pattern2 = Pattern.compile("([-]?[^-+]+)");
    	Matcher matcher2 = pattern2.matcher(pPol);
    	
    	while(matcher2.find())
    	{
    		Matcher matcher = pattern.matcher(matcher2.group(1));
    		int coef = 0, grad = 0, coefIndicator = 1;
    		while(matcher.find())
    		{
    			if(matcher.group(1).contains("x"))
    			{
    				coefIndicator = 0;
    			}
    			if(!matcher.group(1).contains("x") && coefIndicator == 1)
    			{
    				coef = Integer.parseInt(matcher.group(1));
    			}
    			if(!matcher.group(1).contains("x") && coefIndicator == 0)
    			{
    				grad = Integer.parseInt(matcher.group(1));
    				if(coef == 0)
    				{
    					coef = 1;
    				}
    			}
    		}
			this.add(coef, grad);
    	}
    	return this;
    }
    
    public Polinom derivare()
    {
    	for(int i = 0; i < this.monoame.size(); i++) 
    	{
    		this.monoame.get(i).setCoef(this.monoame.get(i).getCoef() * this.monoame.get(i).getGrad());
    		this.monoame.get(i).setGrad(this.monoame.get(i).getGrad() - 1);
    	}
    	return this;
    }
    public Polinom integrare()
    {
    	for(int i = 0; i < this.monoame.size(); i++) 
    	{
    		this.monoame.get(i).setCoef(this.monoame.get(i).getCoef() / (this.monoame.get(i).getGrad() + 1));
    		if(this.monoame.get(i).getCoef().equals(0))
    		{
    			this.monoame.get(i).setCoef(1);
    		}
    		this.monoame.get(i).setGrad(this.monoame.get(i).getGrad() + 1);
    	}
    	return this;
    }
    	
}
