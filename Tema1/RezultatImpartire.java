
public class RezultatImpartire {
	private Polinom cat;
	private Polinom rest;
	
	public RezultatImpartire(Polinom cat, Polinom rest)
	{
		this.cat = cat;
		this.rest = rest;
	}
	
	public Polinom getCat()
	{
		return cat;
	}
	
	public Polinom getRest()
	{
		return rest;
	}
	
}
